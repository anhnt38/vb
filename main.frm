VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   7935
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   17565
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   17565
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton btnSave 
      Caption         =   "Luu lai"
      Height          =   615
      Left            =   13680
      TabIndex        =   21
      Top             =   7200
      Width           =   3255
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "Xoa ban ghi"
      Height          =   615
      Left            =   9960
      TabIndex        =   20
      Top             =   7200
      Width           =   3135
   End
   Begin VB.TextBox txtTen 
      Height          =   465
      Left            =   1680
      TabIndex        =   18
      Top             =   4440
      Width           =   4455
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   1680
      TabIndex        =   17
      Top             =   6600
      Width           =   4455
   End
   Begin VB.TextBox txtGhiChu 
      Height          =   465
      Index           =   1
      Left            =   10320
      TabIndex        =   15
      Top             =   5880
      Width           =   4455
   End
   Begin VB.OptionButton gtNu 
      Caption         =   "Nu"
      Height          =   255
      Left            =   4200
      TabIndex        =   9
      Top             =   6000
      Width           =   1695
   End
   Begin VB.OptionButton gtNam 
      Caption         =   "Nam"
      Height          =   255
      Left            =   1680
      TabIndex        =   8
      Top             =   6000
      Value           =   -1  'True
      Width           =   1935
   End
   Begin VB.TextBox txtNoiCongTac 
      Height          =   465
      Left            =   10320
      TabIndex        =   6
      Top             =   4440
      Width           =   4455
   End
   Begin VB.TextBox txtDiaChi 
      Height          =   465
      Left            =   10320
      TabIndex        =   5
      Top             =   3720
      Width           =   4455
   End
   Begin VB.TextBox txtEmail 
      Height          =   465
      Left            =   1680
      TabIndex        =   4
      Top             =   5160
      Width           =   4455
   End
   Begin VB.TextBox txtNgaySinh 
      Height          =   465
      Index           =   0
      Left            =   10320
      TabIndex        =   3
      Top             =   5160
      Width           =   4455
   End
   Begin VB.TextBox txtSoDT 
      Height          =   465
      Left            =   1680
      TabIndex        =   1
      Top             =   3720
      Width           =   4455
   End
   Begin VB.CommandButton btnThemMoi 
      Caption         =   "Them moi"
      Height          =   555
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   7320
      Width           =   3195
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "main.frx":0000
      Height          =   3360
      Left            =   120
      TabIndex        =   19
      Top             =   15
      Width           =   18015
      _ExtentX        =   31776
      _ExtentY        =   5927
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataMember      =   "DT"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label8 
      Caption         =   "Ghi chu"
      Height          =   255
      Index           =   1
      Left            =   8520
      TabIndex        =   16
      Top             =   6000
      Width           =   1335
   End
   Begin VB.Label Label8 
      Caption         =   "Ngay sinh"
      Height          =   255
      Index           =   0
      Left            =   8520
      TabIndex        =   14
      Top             =   5280
      Width           =   1335
   End
   Begin VB.Label Label7 
      Caption         =   "Noi Cong tac"
      Height          =   375
      Left            =   8520
      TabIndex        =   13
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label Label6 
      Caption         =   "Dia chi"
      Height          =   255
      Left            =   8520
      TabIndex        =   12
      Top             =   3840
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "Gioi tinh"
      Height          =   495
      Left            =   120
      TabIndex        =   11
      Top             =   5880
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Email"
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   5160
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Ten"
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   4560
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "So DT"
      Height          =   300
      Left            =   120
      TabIndex        =   2
      Top             =   3840
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim MinHeight As Long
Dim MinWidth As Long
Dim conn As ADODB.Connection


Private Sub btnDelete_Click()
    DataEnvironment1.rsDT.Delete adAffectCurrent
End Sub


Private Sub btnThemMoi_Click(Index As Integer)
    If txtSoDT.Text = "" And txtTen.Text = "" Then
        MsgBox "Nhap ten va so dien thoai"
        Exit Sub
    End If
    ' add a new entry to our table.
    With DataEnvironment1.rsDT
        .AddNew
        !Ten = txtTen
        !SoDT = txtSoDT
        !Email = txtEmail
        !DiaChi = txtDiaChi
        !NoiCongTac = txtNoiCongTac
        !GhiChu = txtGhiChu
        !GioiTinh = gtNam.Value
        !MaNhom = Split(Combo1.Text, ".")(0)
        .Update
    End With
    
    ' requery the db and re-bind the data source to the data grid
    DataEnvironment1.rsDT.Requery
    Set DataGrid1.DataSource = DataEnvironment1
    Call Form_Resize
    
    ' clear the text fields once the new record is added
    txtTen = ""
    txtSoDT = ""
    txtEmail = ""
    
    ' set the focus back to the  artist name textbox
    txtTen.SetFocus
End Sub

Private Sub Form_Initialize()
    Set conn = New ADODB.Connection
    conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source =" & App.Path & "\danhbadienthoai.mdb"
    conn.Open
    loadCombo
    
End Sub

Private Sub Form_Load()
 ' record the height and size of the window for reference
    MinHeight = Form1.Height
    MinWidth = Form1.Width
    
    ' disable the add button
    'cmdAddEntry.Enabled = False
End Sub

Private Sub Form_Resize()
 ' check to see if the form is getting too small (Note: this is just to avoid
    ' the math necessary to shrink all the textboxes, hahahaha!!)
    If MinHeight > Form1.Height Then
        Form1.Height = MinHeight
        Exit Sub
    ElseIf MinWidth > Form1.Width Then
        Form1.Width = MinWidth
        Exit Sub
    End If
    
    ' resize the flexgrid to fit nicely on the screen
    DataGrid1.Width = Form1.ScaleWidth
    'DataGrid1.Height = Form1.ScaleHeight / 2
End Sub
Public Sub loadCombo()
    Dim rs As New ADODB.Recordset
    'rs.CursorLocation = adUseClient
    rs.Open "SELECT * FROM Nhom", conn
    Do While rs.EOF <> True
        Combo1.AddItem (rs.Fields("MaNhom").Value & "." & rs.Fields("TenNhom").Value)
        rs.MoveNext
    Loop
    Combo1.ListIndex = 0
End Sub
